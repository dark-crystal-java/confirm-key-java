package org.magmacollective.darkcrystal.confirmkey;

/**
 * How a wordlist must be structured
 */
public interface WordList {

  /**
   * Get a word in the word list.
   *
   * @param index Index of word in the word list [0..2047] inclusive.
   * @return the word from the list.
   */
  String getWord(final int index);

  /**
   * Get the space character for this language.
   *
   * @return a whitespace character.
   */
  char getSpace();

  /**
   * Get the number of words in the wordlist
   *
   * @return the number of words in the list
   */
  int getLength();
}
