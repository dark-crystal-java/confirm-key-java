package org.magmacollective.darkcrystal.confirmkey;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

/**
 * Provides a method for deriving dictionary words from a given key
 */
public class ConfirmKey {
  /**
   * The maximum number of words we can derive
   */
  public static final int MAXWORDS = 31;

  /**
   * Derive some words from a given key with a given wordslist
   * @param wordList a WordList in the chosen language
   * @param numWords the number of words to derive
   * @param key the byte array from which to derive words.
   * @return The derived words
   * @throws Exception if there was a problem with the word list
   */
  public static String getWords(WordList wordList, int numWords, byte[] key) throws Exception {
    if (numWords < 0 || numWords > MAXWORDS) throw new Exception("number of words must be between 0 and " + MAXWORDS);
    byte[] hash = MessageDigest.getInstance("SHA-256").digest(key);
    ByteBuffer hashByteBuffer = ByteBuffer.wrap(hash);
    String words = "";
    for (int i = 0; i < numWords; i++) {
      short num = hashByteBuffer.getShort(i);
      int intNum = (num + Short.MAX_VALUE + 1) % wordList.getLength();
      words += wordList.getWord(intNum) + wordList.getSpace();
    }
    return words.substring(0, words.length() -1);
  }
}
