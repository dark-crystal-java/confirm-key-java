package org.magmacollective.darkcrystal.confirmkey;

import org.junit.jupiter.api.Test;

import java.security.SecureRandom;

import static org.junit.jupiter.api.Assertions.*;

class ConfirmKeyTest {
  @Test void testGetWordsChineseSimplified() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(ChineseSimplified.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(ChineseSimplified.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsChineseTraditional() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(ChineseTraditional.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(ChineseTraditional.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsEnglish() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(English.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(English.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsFrench() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(French.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(French.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsItalian() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(Italian.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(Italian.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsJapanese() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(Japanese.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(Japanese.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsKorean() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(Korean.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(Korean.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }

  @Test void testGetWordsSpanish() throws Exception {
    SecureRandom random = new SecureRandom();
    byte[] key = new byte[64];
    random.nextBytes(key);
    String words = ConfirmKey.getWords(Spanish.INSTANCE, 3, key);
    System.out.println(words);
    assertTrue(words.length() > 0, "Words has length");
    assertTrue(words.split(String.valueOf(Spanish.INSTANCE.getSpace())).length == 3, "Correct number of words");
  }
}
