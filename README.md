# Confirm Key

Generate a short mnemonic from a key, to help with verbally confirming that two keys are the same.

## Why?

Some times it is important to confirm that a key, hash, signature, or other kind of binary data has been successfully transmitted or retrieved.  But reading 'random' strings of characters is difficult, especially if they need to be confirm over an audio call, as many characters are phonetically similar when pronounced.

This module derives some dictionary words from a given key for this purpose.

The wordlists included are from BIP39 which are designed to be commonly understood words which are phonetically distinct. Of the latin languages, all words have a unique four character prefix, meaning only the first four characters are needed, the remaining character serve to help us recognise the word.

BIP39 wordlists are provided in:
- Simplified Chinese
- Traditional Chinese
- English
- French
- Italian
- Japanese
- Korean
- Spanish

Additional wordlists can be added.

## Example

```java
import org.magmacollective.darkcrystal.confirmkey.English;
import org.magmacollective.darkcrystal.confirmkey.getWords;

System.out.println ConfirmKey.getWords(English.INSTANCE, 3, "This is the key".getBytes());
// output: blossom addict eye
```
